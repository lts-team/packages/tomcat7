# Portuguese/Brazil translation of tomcat7.
# Copyright (C) 2010 THE tomcat7'S COPYRIGHT HOLDER
# This file is distributed under the same license as the tomcat7 package.
# José de Figueiredo <deb.gnulinux@gmail.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: tomcat7\n"
"Report-Msgid-Bugs-To: tomcat7@packages.debian.org\n"
"POT-Creation-Date: 2010-08-06 04:08+0200\n"
"PO-Revision-Date: 2011-01-18 09:04-0200\n"
"Last-Translator: José de Figueiredo <deb.gnulinux@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"pt_BR utf-8\n"

#. Type: string
#. Description
#: ../tomcat7.templates:1001
msgid "Dedicated system account for the tomcat7 daemon:"
msgstr "Conta de sistema dedicada para o daemon tomcat7:"

#. Type: string
#. Description
#: ../tomcat7.templates:1001
msgid ""
"The tomcat7 server must use a dedicated account for its operation so that "
"the system's security is not compromised by running it with superuser "
"privileges."
msgstr ""
"O servidor tomcat7 deve usar uma conta dedicada para sua operação, desta "
"forma a segurança do sistema não será comprometida por rodar com privilégios "
"de superusuário."

#. Type: string
#. Description
#: ../tomcat7.templates:2001
msgid "Dedicated system group for the tomcat7 daemon:"
msgstr "Grupo de sistema dedicado para o daemon tomcat7:"

#. Type: string
#. Description
#: ../tomcat7.templates:2001
msgid ""
"The tomcat7 server must use a dedicated group for its operation so that the "
"system's security is not compromised by running it with superuser privileges."
msgstr ""
"O servidor tomcat7 deve usar um grupo dedicado para sua operação, desta "
"forma a segurança do sistema não será comprometida por rodar com privilégios "
"de superusuário."

#. Type: string
#. Description
#: ../tomcat7.templates:3001
msgid "Please choose the tomcat7 JVM Java options:"
msgstr "Por favor, escolha as opções Java da JVM para o tomcat7:"

#. Type: string
#. Description
#: ../tomcat7.templates:3001
msgid "Tomcat's JVM will be launched with a specific set of Java options."
msgstr ""
"A JVM para o tomcat7 iniciará com um conjunto específico de opções Java."

#. Type: string
#. Description
#: ../tomcat7.templates:3001
msgid ""
"Note that if you use -XX:+UseConcMarkSweepGC you should add the -XX:"
"+CMSIncrementalMode option if you run Tomcat on a machine with exactly one "
"CPU chip that contains one or two cores."
msgstr ""
"Note que se você usar -XX:+UseConcMarkSweepGC você deverá adicionar a opção -"
"XX:+CMSIncrementalMode se você rodar o Tomcat em uma máquina com exatamente "
"um chip CPU que contenha um ou dois núcleos."
