#!/usr/bin/make -f

export JAVA_HOME := /usr/lib/jvm/default-java

BLDLIB    := output/build/lib
BLDBIN    := output/build/bin
VERSION   := $(shell dpkg-parsechangelog --show-field Version | sed 's/-[^-]*$$//' | sed 's/~/-/')
T_JARS    := tomcat-i18n-fr tomcat-i18n-es tomcat-i18n-ja catalina-ant
T_MAVENIZED_JARS := jasper-el annotations-api jasper catalina-ha catalina
DATE      := $(shell dpkg-parsechangelog --show-field Date)
ANT       := ant -propertyfile debian/ant.properties \
             -Dyear=$(shell date --date='$(DATE)' --utc +'%Y') \
             -Dtoday='$(shell LC_ALL=C date --date='$(DATE)' --utc +'%b %-d %Y')' \
             -Dtstamp=$(shell date --date='$(DATE)' --utc +%H:%M:%S) \
             -Ddistribution.name=$(shell lsb_release -si)

%:
	dh $@

override_dh_auto_build:
	$(ANT) extras-jmx-remote deploy

	# Build the Javadoc for the Servlet, JSP, EL and WebSocket APIs
	javadoc -locale en -subpackages "javax.servlet" -d "output/api" \
		-sourcepath "java" -author -version -breakiterator -notimestamp \
		-windowtitle "Tomcat API Documentation" -doctitle "Tomcat API" \
		-bottom "Copyright &#169; 2000-2016 Apache Software Foundation. All Rights Reserved."

override_dh_auto_test:
#ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
#	mkdir -p webapps/examples/WEB-INF/lib
#	cp /usr/share/java/jstl1.1.jar webapps/examples/WEB-INF/lib/jstl.jar
#	cp /usr/share/java/standard.jar webapps/examples/WEB-INF/lib/standard.jar
#	$(ANT) test
#endif

override_dh_install-indep:
	dh_install -i --exclude=.bat --exclude=Thumbs.db

	mkdir -p debian/poms
	cp res/maven/*.pom debian/poms
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/3.0/'      \
	              debian/poms/tomcat-servlet-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/3.0/'      \
	              debian/javaxpoms/servlet-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/2.2/'      \
	              debian/poms/tomcat-el-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/2.2/'      \
	              debian/javaxpoms/el-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/2.2/'      \
	              debian/poms/tomcat-jsp-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/2.2/'      \
	              debian/javaxpoms/jsp-api.pom
	perl -p -i -e 's/\@MAVEN.DEPLOY.VERSION\@/$(VERSION)/' \
	              debian/poms/*.pom
#	mh_installpoms -plibtomcat7-java
#	for i in $(T_MAVENIZED_JARS); \
#	do \
#		mh_installjar -plibtomcat7-java -l           \
#	                      debian/poms/tomcat-$$i.pom     \
#	                      $(BLDLIB)/$$i.jar              \
#	                      usr/share/tomcat7/lib/$$i.jar; \
#	done
#	mh_installjar -plibtomcat7-java -l --usj-name=catalina-tribes \
#	              debian/poms/tomcat-tribes.pom \
#	              $(BLDLIB)/catalina-tribes.jar \
#	              usr/share/tomcat7/lib/catalina-tribes.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-api \
#	              debian/poms/tomcat-api.pom \
#	              $(BLDLIB)/tomcat-api.jar \
#	              usr/share/tomcat7/lib/tomcat-api.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-coyote \
#	              debian/poms/tomcat-coyote.pom \
#	              $(BLDLIB)/tomcat-coyote.jar \
#	              usr/share/tomcat7/lib/tomcat-coyote.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-juli \
#	              debian/poms/tomcat-juli.pom \
#	              $(BLDBIN)/tomcat-juli.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-util \
#	              debian/poms/tomcat-util.pom \
#	              $(BLDLIB)/tomcat-util.jar \
#	              usr/share/tomcat7/lib/tomcat-util.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-jdbc \
#	              debian/poms/tomcat-jdbc.pom \
#	              output/jdbc-pool/tomcat-jdbc.jar \
#	              usr/share/tomcat7/lib/tomcat-jdbc.jar
#	mh_installjar -plibtomcat7-java -l --usj-name=tomcat-catalina-jmx-remote \
#	              debian/poms/tomcat-catalina-jmx-remote.pom \
#	              output/extras/catalina-jmx-remote.jar \
#	              usr/share/tomcat7/lib/catalina-jmx-remote.jar

#	for i in $(T_JARS); do \
#		mv $(BLDLIB)/$$i.jar $(BLDLIB)/$$i-$(VERSION).jar && \
#		dh_install -plibtomcat7-java \
#			$(BLDLIB)/$$i-$(VERSION).jar usr/share/java && \
#		dh_link -plibtomcat7-java usr/share/java/$$i-$(VERSION).jar \
#			usr/share/java/$$i.jar && \
#		dh_link -ptomcat7-common usr/share/java/$$i-$(VERSION).jar \
#			usr/share/tomcat7/lib/$$i.jar; done

	mh_installpoms -plibservlet3.0-java
	mh_installjar -plibservlet3.0-java -l -s         \
	              debian/poms/tomcat-servlet-api.pom \
	              output/build/lib/servlet-api.jar
	mh_installjar -plibservlet3.0-java -l -s         \
	              debian/javaxpoms/servlet-api.pom \
	              output/build/lib/servlet-api.jar
	mh_installjar -plibservlet3.0-java -l -s         \
	              debian/poms/tomcat-jsp-api.pom     \
	              output/build/lib/jsp-api.jar
	mh_installjar -plibservlet3.0-java -l -s         \
	              debian/javaxpoms/jsp-api.pom     \
	              output/build/lib/jsp-api.jar
	mh_installjar -plibservlet3.0-java -l -s         \
                      debian/poms/tomcat-el-api.pom      \
                      output/build/lib/el-api.jar
	mh_installjar -plibservlet3.0-java -l -s         \
                      debian/javaxpoms/el-api.pom      \
                      output/build/lib/el-api.jar

	# update the checksum for the root webapp
#	unset rwmd5sum \
#		&& rwmd5sum=`cat debian/default_root/index.html debian/default_root/META-INF/context.xml | md5sum - 2>/dev/null | cut -d " " -f1` \
#		&& sed "s/\@ROOT_WEBAPP_MD5SUM\@/$$rwmd5sum/" debian/tomcat7.postrm.in > debian/tomcat7.postrm
	jh_manifest

override_dh_auto_clean:
	dh_auto_clean
	-$(ANT) clean
	rm -rf "output/"
	rm -rf webapps/examples/WEB-INF/lib/*.jar
	rm -f modules/jdbc-pool/output/resources/MANIFEST.MF
	rm -rf debian/tomcat7.postrm debian/poms
	mh_clean

get-orig-source:
	-uscan --download-version $(VERSION) --force-download --rename
