Source: tomcat7
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: James Page <james.page@ubuntu.com>,
 Miguel Landaeta <nomadium@debian.org>,
 tony mancill <tmancill@debian.org>,
 Jakub Adam <jakub.adam@ktknet.cz>,
 Emmanuel Bourg <ebourg@apache.org>
Build-Depends: default-jdk,
 ant-optional,
 debhelper (>= 10),
 po-debconf,
 libhamcrest-java (>= 1.3)
Build-Depends-Indep: maven-repo-helper,
 libcglib-nodep-java,
 libeasymock-java (>= 3.0),
 libecj-java (>= 3.10.1~),
 libobjenesis-java,
 javahelper,
 junit4,
# libjstl1.1-java,
# libjakarta-taglibs-standard-java,
# lsb-release
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-java/tomcat7.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/tomcat7.git
Homepage: http://tomcat.apache.org

#Package: tomcat7-common
#Architecture: all
#Depends: libtomcat7-java (>= ${source:Version}), ${misc:Depends},
# default-jre-headless | java6-runtime-headless | java6-runtime | java-6-runtime
#Description: Servlet and JSP engine -- common files
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains common files needed by the tomcat7 and tomcat7-user
# packages (Tomcat 6 scripts and libraries).

#Package: tomcat7
#Architecture: all
#Depends: tomcat7-common (>= ${source:Version}), ucf,
#	adduser, ${misc:Depends}
#Recommends: authbind
#Suggests: tomcat7-docs (>= ${source:Version}),
#	tomcat7-admin (>= ${source:Version}),
#	tomcat7-examples (>= ${source:Version}),
#	tomcat7-user (>= ${source:Version}),
#	libtcnative-1 (>= 1.1.32~)
#Description: Servlet and JSP engine
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains only the startup scripts for the system-wide daemon.
# No documentation or web applications are included here, please install
# the tomcat7-docs and tomcat7-examples packages if you want them.
# Install the authbind package if you need to use Tomcat on ports 1-1023.
# Install tomcat7-user instead of this package if you don't want Tomcat to
# start as a service.

#Package: tomcat7-user
#Architecture: all
#Depends: tomcat7-common (>= ${source:Version}), netcat, ${misc:Depends}
#Suggests: tomcat7-docs (>= ${source:Version}),
#	tomcat7-admin (>= ${source:Version}),
#	tomcat7-examples (>= ${source:Version}),
#	tomcat7 (>= ${source:Version})
#Description: Servlet and JSP engine -- tools to create user instances
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains files needed to create a user Tomcat instance.
# This user Tomcat instance can be started and stopped using the scripts
# provided in the Tomcat instance directory.

#Package: libtomcat7-java
#Architecture: all
#Depends: libecj-java (>= 3.10.1~),
#         libcommons-pool-java,
#         libcommons-dbcp-java,
#         libservlet3.0-java (>= ${source:Version}), ${misc:Depends}
#Conflicts: libtomcat6-java
#Suggests: tomcat7 (>= ${source:Version})
#Description: Servlet and JSP engine -- core libraries
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the Tomcat core classes which can be used by other
# Java applications to embed Tomcat.

Package: libservlet3.0-java
Architecture: all
Depends: ${misc:Depends}
Description: Servlet 3.0 and JSP 2.2 Java API classes
 Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
 specifications from Sun Microsystems, and provides a "pure Java" HTTP web
 server environment for Java code to run.
 .
 This package contains the Java Servlet and JSP library.

Package: libservlet3.0-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libservlet3.0-java (>= ${source:Version})
Description: Servlet 3.0 and JSP 2.2 Java API documentation
 Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
 specifications from Sun Microsystems, and provides a "pure Java" HTTP web
 server environment for Java code to run.
 .
 This package contains the documentation for the Java Servlet and JSP library.

#Package: tomcat7-admin
#Architecture: all
#Depends: tomcat7-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- admin web applications
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the administrative web interfaces.

#Package: tomcat7-examples
#Architecture: all
#Depends: tomcat7-common (>= ${source:Version}), ${misc:Depends},
# libjstl1.1-java, libjakarta-taglibs-standard-java
#Description: Servlet and JSP engine -- example web applications
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the default Tomcat example webapps.

#Package: tomcat7-docs
#Section: doc
#Architecture: all
#Depends: tomcat7-common (>= ${source:Version}), ${misc:Depends}
#Description: Servlet and JSP engine -- documentation
# Apache Tomcat implements the Java Servlet and the JavaServer Pages (JSP)
# specifications from Sun Microsystems, and provides a "pure Java" HTTP web
# server environment for Java code to run.
# .
# This package contains the online documentation web application.
